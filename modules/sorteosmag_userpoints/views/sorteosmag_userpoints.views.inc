<?php
/**
 * @file
 * Implements the sorteo userpoints view.
 *
 * @defgroup sorteosmag_userpoints_view_data Sorteo View
 * @ingroup sorteosmag_userpoints
 * @{
 * View from sorteosmag_userpoints.
 */

/**
 * Implements hook_views_data().
 */
function sorteosmag_userpoints_views_data() {
  $data['sorteosmag_userpoints']['table']['group'] = t('Sorteo');
  $data['sorteosmag_userpoints']['table']['base'] = array(
    'field' => 'sorteo_id',
    'title' => t('Id of Sorteo'),
    'help' => t('Id of Sorteos.'),
    'weight' => -10,
  );
  $data['sorteosmag_userpoints']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'sorteo_id',
    ),
  );
  $data['sorteosmag_userpoints']['sorteo_id'] = array(
    'title' => t('Id of Node Sorteo'),
    'help' => t('Id of Node sorteo.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id of node in SoretosMag Userpoints'),
      'title' => t('Id of node in SorteosMag Userpoints'),
      'help' => t(''),
    ),
  );
  $data['sorteosmag_userpoints']['userpoints'] = array(
    'title' => t('Quantity of userpoints'),
    'help' => t('Quantity of userpoints.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}
/**
 * @} End of "defgroup sorteo_userpoints_view".
 */

