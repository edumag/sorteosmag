# Módulo sorteosmag_rules

Desde el módulo hemos generado un evento "sorteosmag_contest_status_changed"
que se lanza al cambiar el estado de una instancia.

Este evento pasa las variables:

    'sorteo'      => array('type' => 'node',   'label' => t('SorteoMag node')),
    'instance_id' => array('type' => 'text',   'label' => t('Instance ID')),
    'old_status'  => array('type' => 'text',   'label' => t('Old Status')),
    'new_status'  => array('type' => 'text',   'label' => t('New Status')),
    'start_date'  => array('type' => 'text',   'label' => t('Start Date')),
    'end_date'    => array('type' => 'text',   'label' => t('End Date')),

Dos condiciones que pueden utilizarse.

- Comprobar que el nodo es de tipo sorteosmag.
- El sorteo tiene el status.
  Que permite seleccionar uno o varios estados antes de realizar una acción.


## Regla de ejemplo

Con esta regla cualquier instancia que cambie de estado mostrará un mensaje
por pantalla con la información.

Podría facilmente adaptarse para que enviará un correo al administrador cada
vez que una instancia de sorteo cambiara su estado.

	  { "rules_sorteosmag_test" : {
	      "LABEL" : "SorteosMag Test",
	      "PLUGIN" : "reaction rule",
	      "OWNER" : "rules",
	      "REQUIRES" : [ "rules", "sorteosmag_rules" ],
	      "ON" : { "sorteosmag_contest_status_changed" : [] },
	      "DO" : [
	        { "drupal_message" : { "message" : "sorteo:title [sorteo:title]\r\nsorteo:url [sorteo:url]\r\nstart-date:value [start-date:value]\r\nend-date:value [end-date:value]\r\n\r\ninstance-id:value [instance-id:value]\r\nnew-status:value [new-status:value]\r\nold-status:value [old-status:value]\r\n" } }
	      ]
	    }
	  }
