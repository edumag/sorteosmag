<?php
/**
 * @file
 * Provides rules integration
 */

/**
 * Implementation of hook_rules_event_info().
 */
function sorteosmag_rules_rules_event_info() {
  
  return array(
    'sorteosmag_contest_status_changed' => array(
       'label' => t('Contest status was changed'),
       'variables' => array(
          'sorteo'      => array('type' => 'node',   'label' => t('SorteoMag node')),
          'instance_id' => array('type' => 'text',   'label' => t('Instance ID')),
          'old_status'  => array('type' => 'text',   'label' => t('Old Status')),
          'new_status'  => array('type' => 'text',   'label' => t('New Status')),
          'start_date'  => array('type' => 'text',   'label' => t('Start Date')),
          'end_date'    => array('type' => 'text',   'label' => t('End Date')),
       ),
       'group' => 'SorteosMag',
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info().
 */
function sorteosmag_rules_rules_condition_info() {
  return array(
    'sorteosmag_rules_is_contest' => array(
      'label' => t('Check the node is a sorteosmag type'),
      'parameter' => array(
        'sorteo' => array('type' => 'node', 'label' => t('SorteosMag node'))
      ),
      'group' => 'SorteosMag',
    ),
    'sorteosmag_rules_hasstatus' => array(
      'label' => t('Instances has statuses'),
      'arguments' => array(
        'status' => array(
          'type' => 'list<text>', 
          'label' => t('Status of instace'),
          'options list' => 'sorteosmag_rules_status_options_list',
          'restriction' => 'input',
          'multiple' => TRUE,
        ),
        'new_status'  => array(
          'type' => 'value', 
          'label' => t('New Status')),
      ),
      'help' => t('Whether the instance has the selected statuses.'),
      'group' => 'SorteosMag',
      'callbacks' => array(
      'execute' => 'sorteosmag_rules_hasstatus_validation',
    ),
    ),
  );
}

/**
 * Rules Condition is_contest
 */
function sorteosmag_rules_is_contest($node) {
  return sorteosmag_is_sorteosmag($node);
}

/**
 * Rules Condition status
 */
function sorteosmag_rules_hasstatus_validation($status, $new_status) {
  debug($status);
  debug($new_status);
  foreach($statuses as $status){
    if ($new_status == $status){
      return TRUE;
    }
  }
  return FALSE;
}
/**
 * Condition: condition to check whether contest has status
 */
function sorteosmag_rules_status_options_list($instance_id, $settings) {
  return sorteosmag_instances_status_array();
  
}

