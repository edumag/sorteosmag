<?php
/**
 * @file sorteosmag_repeat.helper.inc
 *
 */

/**
 * Helper function to build repeating dates from a $node_field.
 *
 * Pass in either the RRULE or the $form_values array for the RRULE,
 * whichever is missing will be created when needed.
 *
 * @todo Clean.
 */
function sorteosmag_repeat_date_repeat_build_dates($settings, $rrule, $rrule_values) {

  $days_register = $settings['repeat']['schedule']['options']['days_register'];

  // The first date is always the current day. Not useful.
  // Add one day rule_values ['count'] and not count the first.
  if ( isset($rrule_values['COUNT']) ) {
    $rrule_values['COUNT']++ ;
    //$rrule = date_api_ical_build_rrule($rrule_values);
    $rrule = NULL;
  }

  $date_field['type'] = DATE_DATETIME;
  $date_field['field_name'] = 'temporal';
  $date_field['settings']['tz_handling'] = 'site';
  $date_field['settings']['granularity'] = array('year', 'month', 'day', 'hour', 'minute', 'second');

  //$tz = $settings['tz_handling'];
  $tz = date_default_timezone_get();

  $timezone = date_get_timezone($tz, '');
  $timezone_db = date_get_timezone_db($tz);
  
  // Start date is now less number of days to register.
  $start = new DateObject(time()-(60 * 60 * 24 * $days_register), $timezone);
  $start->granularity = array('year', 'month', 'day', 'hour', 'minute', 'second', 'timezone');
  $end   = date_now(); 

  if ($timezone != $timezone_db) {
    date_timezone_set($start, timezone_open($timezone));
  }

  $item = array(
    'timezone' => $tz,
    //'value' => $start->format(date_type_format($date_field['type'])),
    'value' => FALSE,
    //'value2' => $end->format(date_type_format($date_field['type'])),
    'value2' => FALSE,
    'rrule' => $rrule,
    'show_repeat_settings' => 1,
    'offset' => 10800,
    'offset2' => 10800,
    'show_todate' => 1,
  );
  
  if (!empty($rrule_values['UNTIL'])) {
    $rrule_values['UNTIL']['datetime'] .= ' 23:59:59';
    $rrule_values['UNTIL']['granularity'] = serialize(drupal_map_assoc(array('year', 'month', 'day', 'hour', 'minute', 'second')));
    $rrule_values['UNTIL']['all_day'] = 0;
  }

  module_load_include('inc', 'date', 'date_repeat');

  // Hay un error en el módulo date_repeat.
  // Al seleccionar por semanas y marcar unicamente el
  // domingo se genera un bucle infinito.
  // Para evitarlo cambio el domingo por el lunes y
  // después quito un día.
  $restar_dia = FALSE;
  if ( $rrule_values['FREQ'] == 'WEEKLY' 
    && ! empty($rrule_values['BYDAY']['SU']) 
    && empty($rrule_values['BYDAY']['MO']) 
    && empty($rrule_values['BYDAY']['TU']) 
    && empty($rrule_values['BYDAY']['WE']) 
    && empty($rrule_values['BYDAY']['TH']) 
    && empty($rrule_values['BYDAY']['FR']) 
    && empty($rrule_values['BYDAY']['SA']) 
  ) {
    $rrule_values['BYDAY']['SU'] = FALSE;
    $rrule_values['BYDAY']['MO'] = 'MO';
    $restar_dia = TRUE;
  }
  $values = @date_repeat_build_dates($rrule = FALSE, $rrule_values, $date_field, $item);

  $query = db_insert('sorteosmag_instance')
    ->fields(array('sorteo_id', 'start_date', 'end_date', 'status'));
  
  $count = 0;
  foreach ($values as $delta => $date) {
    if ( $count !== 0 ) {
      $end_date = $date['value2'];
      if ( $restar_dia ) {
        $end_date = strtotime ( '-1 day' , strtotime ( $end_date ) ) ;
        $end_date = date ( 'Y-m-d H:i' , $end_date );
      }
      $start_date = strtotime ( '-' . $days_register .' day' , strtotime ( $end_date ) ) ;
      $start_date = date ( 'Y-m-d' , $start_date );
      $query->values(array($settings['nid'], $start_date, $end_date, SORTEOSMAG_INSTANCES_STATUS_WAITING));
    } 
    $count++;
  }
  
  $query->execute();

  $msg = t('%count new instances have been added', array(
    '%count' => ( $count -1 ),
    )
  );
  drupal_set_message($msg);
  return $values;
  
}
