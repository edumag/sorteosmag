<?php
/**
 * @file
 */

function sorteosmag_instances_status_array() {
 return $options = array(
  SORTEOSMAG_INSTANCES_STATUS_DISABLED => t('Deactivated'),
  SORTEOSMAG_INSTANCES_STATUS_WAITING  => t('Waiting to start date'),
  SORTEOSMAG_INSTANCES_STATUS_OPEN     => t('Subscriptions open'),
  SORTEOSMAG_INSTANCES_STATUS_CLOSE    => t('Subscriptions closed'),
  SORTEOSMAG_INSTANCES_STATUS_WINNERS  => t('Winners assigned'),
  SORTEOSMAG_INSTANCES_STATUS_END      => t('Completed'),
  SORTEOSMAG_INSTANCES_STATUS_CANCELED => t('Canceled'),
 );
}
function sorteosmag_instances_status_name($status) {
  
  static $options = array(
    SORTEOSMAG_INSTANCES_STATUS_DISABLED => 'Deactivated',
    SORTEOSMAG_INSTANCES_STATUS_WAITING  => 'Waiting',
    SORTEOSMAG_INSTANCES_STATUS_OPEN     => 'Open',
    SORTEOSMAG_INSTANCES_STATUS_CLOSE    => 'Closed',
    SORTEOSMAG_INSTANCES_STATUS_WINNERS  => 'Winners',
    SORTEOSMAG_INSTANCES_STATUS_END      => 'Completed',
    SORTEOSMAG_INSTANCES_STATUS_CANCELED => 'Canceled',
 );
  if (!isset($options[$status])) {
    return '';
  }
  return $options[$status]; // t() not used here
}

function sorteosmag_is_sorteosmag($obj) {
  
  $type = $obj;
  
  if (is_object($type)) {
    $type = $type->type;
  }
  if (is_array($type)) {
    $type = $type['type'];
  }
  return ( $type === 'sorteosmag' );
}

/**
 * Change status instantace.
 */
function _sorteosmag_change_status_instance($instance_id, $new_status) {

  global $language;

  $settings = sorteosmag_get_settings();

  $instance = _sorteosmag_instance_get($instance_id);
  $old_status = $instance['status'];
  $sorteo_id  = $instance['sorteo_id'];
  $start_date = $instance['start_date'];
  $end_date   = $instance['end_date'];
  $sorteo = node_load($sorteo_id);

  if ( $old_status == $new_status ) return;

  $query = db_update('sorteosmag_instance')
    ->condition('id', $instance_id)
    ->fields(array(
      'status' => $new_status,)
    );
  $query->execute();

  module_invoke_all('sorteosmag', 'sorteosmag_instance_status_changed', 
    $sorteo,
    $instance_id,
    $old_status,
    $new_status,
    $start_date,
    $end_date
  );

  // Check is the ultimate.
  if ( $new_status == SORTEOSMAG_INSTANCES_STATUS_END || 
       $new_status == SORTEOSMAG_INSTANCES_STATUS_CANCELED
  ) {
    
    $query = db_select('sorteosmag_instance', 'si')
      ->fields('si', array(
        'id',
      ));
    $query->where('si.start_date > NOW() AND si.sorteo_id = ' . $sorteo_id);
    $results = $query->execute(); 
    $count_future_instances = $results->rowCount();

    if ( $count_future_instances < 1 ) {
      // Send email to owner.
      if($settings['email_notice_to_owner_last_contest']) {
        $params = array(
          'sorteo-node'      => $sorteo, 
          'sorteo-instance'  => $instance, 
          'settings'         => $settings, 
        );
        $admin = user_load($sorteo->uid);
        drupal_mail('sorteosmag', 'owner_mail_last_instance', $admin->mail, $language, $params);
      }
    }
  }

  // Closed.
  // La fecha de la ejecución del sorteo ha pasado.
  if ( $new_status == SORTEOSMAG_INSTANCES_STATUS_CLOSE ) {

    $automatic = ( $sorteo->sorteosmag['automatic'] == 1 ) ? TRUE : FALSE ;

    // Randomly generate winners.
    if ($automatic) {
      _sorteosmag_generate_winners_rand($instance_id, $sorteo_id);
    } 
    // Tell the administrator to run contest.
    else {
      if($settings['email_to_tell_run_admin']) {
        $params = array(
          'sorteo-node'      => $sorteo, 
          'sorteo-instance'  => $instance, 
          'settings'         => $settings, 
        );
        $admin = user_load($sorteo->uid);
        drupal_mail('sorteosmag', 'admin_tell_run_contest_mail', $admin->mail, $language, $params);
      }
    }
  } 

  // Winners.
  // Tenemos ganadores.
  elseif ( $new_status == SORTEOSMAG_INSTANCES_STATUS_WINNERS ) {
    _sorteosmag_change_status_instance($instance_id, SORTEOSMAG_INSTANCES_STATUS_END);
  }

  // Completed.
  elseif ( $new_status == SORTEOSMAG_INSTANCES_STATUS_END ) {
    if($settings['email_to_winner']) {
      $new_winners = _sorteosmag_winners_instance($instance_id);
      foreach($new_winners as $winners => $info) {
        $account = user_load($info->user_id);
        $prize   = node_load($info->prize_id);
        if (!$account) {
          return;
        }
        $params = array(
          'sorteo-node'      => $sorteo, 
          'sorteo-instance'  => $instance, 
          'prize-node'       => $prize, 
          'contestant-user'  => $account,
          'settings'         => $settings,
        );
        drupal_mail('sorteosmag', 'winners_mail', $account->mail, $language, $params);
      }
    }
    if($settings['email_to_participants']) {
      $new_winners = db_query("SELECT user_id, prize_id FROM {sorteosmag_contestant} 
        WHERE instance_id = :instance_id AND prize_id IS NULL", array(
          ':instance_id' => $instance_id,
          )
        );
      foreach($new_winners as $winners => $info) {
        $account = user_load($info->user_id);
        $prize   = node_load($info->prize_id);
        if (!$account) {
          return;
        }
        $params = array(
          'sorteo-node'      => $sorteo, 
          'sorteo-instance'  => $instance, 
          'prize-node'       => $prize, 
          'contestant-user'  => $account,
          'settings'         => $settings,
        );
        drupal_mail('sorteosmag', 'email_to_participants', $account->mail, $language, $params);
      }
    }
    if($settings['email_to_admin']) {
      $params = array(
        'sorteo-node'      => $sorteo, 
        'sorteo-instance'  => $instance, 
        'settings'         => $settings, 
      );
      $admin = user_load($sorteo->uid);
      drupal_mail('sorteosmag', 'admin_winners_mail', $admin->mail, $language, $params);
    }
  }

}

/**
 * Get instances.
 */
function _sorteosmag_get_instances($status = FALSE, $sorteo_id = FALSE) {
  $sql = 'SELECT * FROM {sorteosmag_instance} WHERE 1 ';
  if ( $status )    $sql .= 'AND status = ' . $status ;
  if ( $sorteo_id ) $sql .= 'AND sorteo_id = ' . $sorteo_id ;

  return db_query($sql); 
} 

/**
 * Insert instance or update.
 */
function _sorteosmag_insert_instance($nid, $values, $instance_id = FALSE) {

  $sorteo_id  = $nid;
  $start_date = $values['startdate'];
  $end_date   = $values['enddate'];
  $status     = SORTEOSMAG_INSTANCES_STATUS_WAITING;
  $start_date_unix = strtotime($start_date);
  $end_date_unix   = strtotime($end_date);

  if ( $start_date_unix < time() ) $status = SORTEOSMAG_INSTANCES_STATUS_OPEN;
  if ( $end_date_unix < time() ) $status = SORTEOSMAG_INSTANCES_STATUS_CLOSE;

  try {

    if ( $instance_id ) {
      $query = db_update('sorteosmag_instance')
        ->condition('id', $instance_id)
        ->fields(array(
          'start_date' => $start_date,
          'end_date' => $end_date,
          // 'status' => $status,
          )
        );
      $query->execute();
      _sorteosmag_change_status_instance($instance_id, $status);
  
    }
    else {
      $query = db_insert('sorteosmag_instance')
        ->fields(array('sorteo_id', 'start_date', 'end_date', 'status'));

      $query->values(array($sorteo_id, $start_date, $end_date, $status));
      $query->execute();
    }

  }
  catch (Exception $e) {
    drupal_set_message(t('%message',
      array('%message' => $e->getMessage())), 'error');
    return FALSE;
  }
  return $values;

}
/**
 * Get winners ofinstance.
 *
 * @param int $instance_id
 *
 * @return array winners.
 */
function _sorteosmag_winners_instance($instance_id) {

  try {

    $winners = db_query("SELECT user_id, prize_id FROM {sorteosmag_contestant} 
      WHERE instance_id = :instance_id AND prize_id IS NOT NULL AND prize_id != 'NULL' ", array(
        ':instance_id' => $instance_id,
        )
    );


  }
  catch (Exception $e) {

    drupal_set_message(t('%message',
      array('%message' => $e->getMessage())), 'error');
    return FALSE;

  }

  return $winners;

  
}
/**
 * Insert winner in instance of draw.
 */
function _sorteosmag_insert_winner($instance_id, $user_id, $prize_id) {

  try {

    $query = db_update('sorteosmag_contestant')
      ->condition('user_id', $user_id)
      ->condition('instance_id', $instance_id)
      ->fields(array(
        'prize_id' => $prize_id,
      ));

    // dpq($query);
    $result = $query->execute();

  }
  catch (Exception $e) {

    drupal_set_message(t('%message',
      array('%message' => $e->getMessage())), 'error');
    return FALSE;

  }
  return TRUE;
}
/**
 * Randomly generate winners.
 */
function _sorteosmag_generate_winners_rand($instance_id, $sorteo_id){

  $prizes       = FALSE;
  $winners_rand = FALSE;

  // Prizes.
  $query = db_query("SELECT prize_id, quantity FROM {sorteosmag_prize} 
    WHERE sorteo_id = :sorteo_id ",
      array(':sorteo_id' => $sorteo_id));

  foreach ( $query as $result ) {
    $prizes[] = array('prize' => $result->prize_id, 'quantity' => $result->quantity);
  }

  if ( ! $prizes ) {
    watchdog('sorteosmag', "Impossible to contest without prizes, instance:[".$instance_id."]");
    return FALSE;
  }

  // Total numbre of prixes.
  $total_number_prizes = 0;
  foreach ($prizes as $prize) {
    $total_number_prizes = $total_number_prizes + $prize['quantity'];
  }

  // Winners.
  $query = db_query("SELECT user_id FROM {sorteosmag_contestant} 
    WHERE instance_id = :instance_id ORDER BY RAND() LIMIT " . $total_number_prizes,
      array(':instance_id' => $instance_id));

  foreach ( $query as $result ) {
    $winners_rand[] = $result->user_id;
  }

  //if ( ! $winners_rand || count($winners_rand) < $total_number_prizes ) {
  if ( ! $winners_rand ) {
    watchdog('sorteosmag', "Impossible to contest without contestant. Contest canceled, instance:[".$instance_id."]");
    drupal_set_message(t("Impossible to contest without contestant. Contest canceled"));
    _sorteosmag_change_status_instance($instance_id, SORTEOSMAG_INSTANCES_STATUS_CANCELED);
    return FALSE;
  }

  $has_winner = FALSE;
  $count = 0;
  foreach ( $prizes as $prize ) {
    $prize_id = $prize['prize'];
    $quantity = $prize['quantity'];
    for($i=0 ; $i < $quantity ; $i++){
      if ( ! isset($winners_rand[$count]) ) {
        // If no other contestants jump.
        break;
      }
      $user_id = $winners_rand[$count];
      $user = user_load($user_id);
      if (!_sorteosmag_insert_winner($instance_id, $user_id, $prize_id)) {
        $msg = t('The user @name not has been registered like winner in instance @instance_id.',
          array(
            '@name' => $user->name,
            '@instance_id' => $instance_id,
          )
        );
        watchdog('sorteosmag',
          $msg,
          NULL,
          WATCHDOG_ERROR
        );
        return FALSE;
      }
      else {
        $has_winner = TRUE;
        $msg = t('The user @name has been registered like winner in instance @instance_id.',
          array(
            '@name' => $user->name,
            '@instance_id' => $instance_id,
          )
        );
        watchdog('sorteosmag',
          $msg,
          NULL,
          WATCHDOG_INFO
        );
      }
    $count++;
    }
  }

  if ( $has_winner ) {
    _sorteosmag_change_status_instance($instance_id, SORTEOSMAG_INSTANCES_STATUS_END);
  } 
  else {
    return FALSE;
  }
}
/**
 * Insert contestant in instance of draw.
 */
function _sorteosmag_insert_contestant($instances_id, $user_id) {

  try {

    $query = db_insert('sorteosmag_contestant')
      ->fields(array('instance_id', 'user_id'));

    $query->values(array($instances_id, $user_id));
    $result = $query->execute();
    module_invoke_all('insert_contestant', $result);
  }
  catch (Exception $e) {

    drupal_set_message(t('%message',
      array('%message' => $e->getMessage())), 'error');
    return FALSE;

  }

  return TRUE;

  
}

function _sorteosmag_number_contestant($instance_id){

  $query = db_select('sorteosmag_contestant', 'sc')
    ->fields('sc', array(
      'user_id',
    ));
  $query->where('sc.instance_id IN (:nids)', array(':nids' => $instance_id));

  $results = $query->execute();
  return $results->rowCount();
}

function _sorteosmag_is_contestant_instance($instance_id, $uid){
  $query = db_select('sorteosmag_contestant', 'sc')
    ->fields('sc', array(
      'user_id',
    ));
  $query->where('sc.instance_id IN (:nids)', array(':nids' => $instance_id));
  $query->where('sc.user_id = :uid', array(':uid' => $uid));
  $results = $query->execute();
  $return  = $results->rowCount();
  return ( $return > 0 ) ? TRUE : FALSE ;
}
/**
 * Get instance.
 *
 * @param int $instances_id
 *   Instance Id.
 *
 * @return array Instance data.
 */
function _sorteosmag_instance_get($instance_id){
  
  $query = db_select('sorteosmag_instance', 'si')
    ->fields('si', array(
      'id',
      'sorteo_id',
      'start_date',
      'end_date',
      'status',
    ));
  $query->groupBy('si.id');
  $query->where('si.id IN (:nids)', array(':nids' => $instance_id));

  return $query->execute()->fetchAssoc();

}

function _sorteosmag_prizes_draw($sorteo_id){

  $query = db_select('sorteosmag_prize', 'sp')
    ->condition('sorteo_id', $sorteo_id)
    ->fields('sp', array(
      'prize_id',
      'quantity',
      ))
    ->execute();

  return $query;
}
/**
 * Returns the sorteosmag' settings
 * @return array
 */
function sorteosmag_get_settings() {
  static $settings; 
  
  $default_settings = array(
    'email_to_winner' => 1,
    'email_to_participants' => 1,
    'email_to_admin' => 1,
    'email_to_tell_run_admin' => 1,
    'email_notice_to_owner_last_contest' => 1,
    'email_to_winner_subject' => t('Congratulations! You got it!'),
    'email_to_winner_body' => t('
Hi [contestant-user:name], 

Congratulations! You are the winner of the contest [sorteo-node:title]. You can see the final results at [site:url]sorteos/[sorteo-instance:id]/view.

Prize: [prize-node:title]
  
--
Thank You
[site:name]
[site:url]
  '),
    'email_to_participants_subject' => t('Final contest results anounced at ![site:name]'),
    'email_to_participants_body' => t('
Hi [contestant-user:name], 

The final results for the contest [sorteo-node:title] has been announced. You can see the final results at [site:url]sorteos/[sorteo-instance:id]/view.	

--
Thank You
[site:name]
[site:url]
  '),
    'email_to_admin_subject' => t('Final contest results anounced at ![site:name]'),
    'email_to_admin_body' => t('
Hi, 

The final results for the contest [sorteo-node:title] has been announced. You can see the final results at [site:url]sorteos/[sorteo-instance:id]/view.

--
Thank You
[site:name]
[site:url]
  '),
    'email_to_tell_run_admin_subject' => t('A new contest is expected to be executed at ![site:name]'),
    'email_to_tell_run_admin_body' => t('
Hi, 

A new contest is expected to be executed.

Contest: [sorteo-node:title].
Instance: [site:url]sorteos/[sorteo-instance:id]/view.

--
Thank You
[site:name]
[site:url]
  '),
    'email_notice_to_owner_last_contest_subject' => t('Last contest made at ![site:name]'),
    'email_notice_to_owner_last_contest_body' => t('
Contest: [sorteo-node:title].

No more instances of scheduled contest.

To add you can go to [site:url]sorteos/[sorteo-instance:id]/view.

--
Thank You
[site:name]
[site:url]
    '),
    );
  
  $new_settings = variable_get('sorteosmag', $default_settings);
  
  $new_settings += $default_settings;
  
  return $new_settings;
}

