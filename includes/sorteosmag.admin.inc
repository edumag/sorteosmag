<?php
/**
 * @file
 * sorteosmag settings form.
 */

/**
 * sorteosmag settings form.
 */
function sorteosmag_config() {
  $settings = sorteosmag_get_settings();
  $form['sc']['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 1,
    '#group' => 'additional_settings',
  );
  $form['sc']['email']['email_to_winner'] = array(
  	'#type' => 'checkbox',
  	'#title' => t('E-mail to winner'),
  	'#default_value' => $settings['email_to_winner'],
  	'#options' => array(0 => 0, 1 => 1),
  	'#required' => FALSE,
  );
  $form['sc']['email']['email_to_winner_subject'] = array(
  	'#type' => 'textfield',
  	'#title' => t('Winner e-mail subject'),
  	'#default_value' => $settings['email_to_winner_subject'],
  );
  $form['sc']['email']['email_to_winner_body'] = array(
  	'#type' => 'textarea',
  	'#title' => t('Winner e-mail body'),
  	'#default_value' => $settings['email_to_winner_body'],
  	'#rows' => 10,
  );

  $form['sc']['email']['email_to_participants'] = array(
    '#type' => 'checkbox',
    '#title' => t('E-mail to participants'),
    '#default_value' => $settings['email_to_participants'],
    '#options' => array(0 => 0, 1 => 1),
    '#required' => FALSE,
  );
  $form['sc']['email']['email_to_participants_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Participants e-mail subject'),
    '#default_value' => $settings['email_to_participants_subject'],
  );

  $form['sc']['email']['email_to_participants_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Participants e-mail body'),
    '#default_value' => $settings['email_to_participants_body'],
    '#rows' => 10,
  );

  $form['sc']['email']['email_to_admin'] = array(
		'#type' => 'checkbox',
		'#title' => t('E-mail to admin'),
		'#default_value' => $settings['email_to_admin'],
		'#options' => array(0 => 0, 1 => 1),
		'#required' => FALSE,
  );
  $form['sc']['email']['email_to_admin_subject'] = array(
		'#type' => 'textfield',
		'#title' => t('admin e-mail subject'),
		'#default_value' => $settings['email_to_admin_subject'],
  );

  $form['sc']['email']['email_to_admin_body'] = array(
		'#type' => 'textarea',
		'#title' => t('Admin e-mail body'),
		'#default_value' => $settings['email_to_admin_body'],
		'#rows' => 10,
  );

  $form['sc']['email']['email_to_tell_run_admin'] = array(
		'#type' => 'checkbox',
		'#title' => t('E-mail to tell admin run contest'),
		'#default_value' => $settings['email_to_tell_run_admin'],
		'#options' => array(0 => 0, 1 => 1),
		'#required' => FALSE,
  );
  $form['sc']['email']['email_to_tell_run_admin_subject'] = array(
		'#type' => 'textfield',
		'#title' => t('admin to tell run contest e-mail subject'),
		'#default_value' => $settings['email_to_tell_run_admin_subject'],
  );

  $form['sc']['email']['email_to_tell_run_admin_body'] = array(
		'#type' => 'textarea',
		'#title' => t('Admin tell run contest e-mail body'),
		'#default_value' => $settings['email_to_tell_run_admin_body'],
		'#rows' => 10,
  );

  $form['sc']['email']['email_notice_to_owner_last_contest'] = array(
		'#type' => 'checkbox',
		'#title' => t('E-mail notice to owner, last contest'),
		'#default_value' => $settings['email_notice_to_owner_last_contest'],
		'#options' => array(0 => 0, 1 => 1),
		'#required' => FALSE,
  );
  $form['sc']['email']['email_notice_to_owner_last_contest_subject'] = array(
		'#type' => 'textfield',
		'#title' => t('E-mail notice to owner, last contest, subject'),
		'#default_value' => $settings['email_notice_to_owner_last_contest_subject'],
  );

  $form['sc']['email']['email_notice_to_owner_last_contest_body'] = array(
		'#type' => 'textarea',
		'#title' => t('E-mail notice to owner, last contest, body'),
		'#default_value' => $settings['email_notice_to_owner_last_contest_body'],
		'#rows' => 10,
  );

  $form['sc']['email']['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['sc']['email']['token_help']['help'] = array(
  	'#theme' => 'token_tree',
    '#token_types' => array(
      'sorteo-node',
      'sorteo-instance',
      'prize-node',
      'contestant-user',
    ),
  );
  
  $form['sc']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#weight' => 10,
  );
  return $form;
}

/**
 * Validate form config.
 */
function sorteosmag_config_validate($form, &$form_state) {

  $values = $form_state['values'];
  
  
  if( $values['email_to_winner'] && empty($values['email_to_winner_subject'])) {
    form_set_error('email_to_winner_subject', t('If you want to send an email, you should put the subject!'));
  }
  if( $values['email_to_winner'] && empty($values['email_to_winner_body'])) {
    form_set_error('email_to_winner_body', t('If you want to send an email, you should put the body!'));
  }
  if( $values['email_to_participants'] && empty($values['email_to_participants_subject'])) {
    form_set_error('email_to_participants_subject', t('If you want to send an email, you should put the subject!'));
  }
  if( $values['email_to_participants'] && empty($values['email_to_participants_body'])) {
    form_set_error('email_to_participants_body', t('If you want to send an email, you should put the body!'));
  }
  if( $values['email_to_admin'] && empty($values['email_to_admin_subject'])) {
    form_set_error('email_to_admin_subject', t('If you want to send an email, you should put the subject!'));
  }
  if( $values['email_to_admin'] && empty($values['email_to_admin_body'])) {
    form_set_error('email_to_admin_body', t('If you want to send an email, you should put the body!'));
  }
  if( $values['email_to_tell_run_admin'] && empty($values['email_to_tell_run_admin_subject'])) {
    form_set_error('email_to_tell_run__adminsubject', t('If you want to send an email, you should put the subject!'));
  }
  if( $values['email_to_tell_run_admin'] && empty($values['email_to_tell_run_admin_body']) ) {
    form_set_error('email_to_tell_run_admin_body', t('If you want to send an email, you should put the body!'));
  }

}

/**
 * Submit form config.
 */
function sorteosmag_config_submit($form, &$form_state) {

  $values = $form_state['values'];
  
  $settings = sorteosmag_get_settings(TRUE);  

  foreach ($settings as $setting => $default) {
    if (isset($values[$setting])) {
      $settings[$setting] = $values[$setting];
    }
  }
  variable_set('sorteosmag', $settings);
    
  drupal_set_message('Your changes have been saved.');

}
