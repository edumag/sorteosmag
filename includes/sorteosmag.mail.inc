<?php
/**
 * @file
 *
 * sorteosmag.main.inc
 * 
 */

/**
 * Implementation of hook_mail().
 */
function _sorteosmag_mail($key, &$message, $params) {

  global $language;

  $settings = $params['settings'];
  
  // lets others to change the params
  $result = module_invoke_all('sorteosmag', 'mail', $key, $params);
  
  $params += $result;
  
  if ( $key == 'winners_mail' ) {

      $subject = $settings['email_to_winner_subject'];
      $body = $settings['email_to_winner_body'];

      $message['subject'] = token_replace($subject, $params, array('language' => $language));
      $message['body'][] = token_replace($body, $params, array('language' => $language));

      return;
  }
  
  if ( $key == 'admin_winners_mail' ) {
    
      $subject = $settings['email_to_admin_subject'];
      $body = $settings['email_to_admin_body'];
      
      $message['subject'] = token_replace($subject, $params);
      $message['body'][] = token_replace($body, $params);
      
      return;
  }
  
  if ( $key == 'admin_tell_run_contest_mail' ) {
    
      $subject = $settings['email_to_tell_run_admin_subject'];
      $body = $settings['email_to_tell_run_admin_body'];
      
      $message['subject'] = token_replace($subject, $params);
      $message['body'][] = token_replace($body, $params);
      
      return;
  }

  if ( $key == 'email_to_participants' ) {
    
      $subject = $settings['email_to_participants_subject'];
      $body = $settings['email_to_participants_body'];
      
      $message['subject'] = token_replace($subject, $params);
      $message['body'][] = token_replace($body, $params);
      
      return;
  }

  if ( $key == 'owner_mail_last_instance' ) {
    
      $subject = $settings['email_notice_to_owner_last_contest_subject'];
      $body = $settings['email_notice_to_owner_last_contest_body'];
      
      $message['subject'] = token_replace($subject, $params);
      $message['body'][] = token_replace($body, $params);
      
      return;
  }
  
}
