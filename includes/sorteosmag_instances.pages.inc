<?php
/**
 * @file
 * user pages for sorteosmag. 
 */

/**
 * sorteosmag_add_instance.
 */
function sorteosmag_add_instance($form, &$form_state, $node = FALSE, $instance_id = FALSE) {

  $start_date = FALSE;
  $end_date   = FALSE;

  if ( $instance_id ) {
    $instance = _sorteosmag_instance_get($instance_id);
    $start_date = $instance['start_date'];
    $end_date   = $instance['end_date'];
    $node = node_load($instance['sorteo_id']);
    drupal_set_title(t('Edit instance of @title', array('@title' => $node->title)));
    $form['description'] = array(
      '#type' => 'item',
      '#title' => t('Editing instance contes'),
    );
  } 
  else {
    drupal_set_title(t('Insert instance of @title', array('@title' => $node->title)));
    $form['description'] = array(
      '#type' => 'item',
      '#title' => t('Creating a new instance of the draw'),
    );
  }
  
  $nid = $node->vid;

  $form['#nid'] = $nid;
  $form['#instance_id'] = $instance_id;

  $form['startdate'] = array(
    '#type' => 'textfield',
    '#description' => t('Format of date Y-m-d H:M.'),
    '#title' => t('Start date'),
  );
  if ( $start_date ) $form['startdate']['#default_value'] = $start_date;

  $form['enddate'] = array(
    '#type' => 'textfield',
    '#description' => t('Format of date Y-m-d H:M.'),
    '#title' => t('End date'),
  );
  if ( $end_date ) $form['enddate']['#default_value'] = $end_date;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#weight' => 210,
  );
  return $form;

}

/**
 * Validate form add_instance.
 */ 
function sorteosmag_add_instance_validate($form, &$form_state) {
  $startdate = $form_state['values']['startdate'];
  $enddate = $form_state['values']['enddate'];
  if ( empty($startdate) ) {
    form_set_error ( 'start date', t('Should be added starting date.'));
  }
  if ( empty($enddate) ) {
    form_set_error ( 'end date', t('Should be added end date.'));
  }
}

/**
 * Submit sorteosmag_add_instance.
 */
function sorteosmag_add_instance_submit($form, &$form_state) {

  $nid = $form['#nid'];
  $instance_id = $form['#instance_id'];

  if ( _sorteosmag_insert_instance($nid, $form_state['values'], $instance_id) ) {
    if ( $instance_id ) {
      drupal_set_message(t('The instance has been update.'));
    }
    else {
      drupal_set_message(t('The instance has been insert.'));
    }
  }

  if ( $instance_id ) {
    drupal_goto('sorteos/' . $instance_id . '/view');
  }
}
/**
 * sorteosmag_instances_page
 */
function sorteosmag_instances_page($node) {
  $output = array();

  drupal_set_title(t('Instances contest of @title', array('@title' => $node->title)));

  if (!sorteosmag_is_sorteosmag($node)){
    return $output;
  }

  $results_display_view = 'instances_of_draw';
  $results = views_embed_view($results_display_view, 'default', $node->nid);

  $output['instances'] = array (
    '#markup' => theme('sorteosmag_instances_display', array(
      'node' => $node, 
      'results' => $results,
      )
    )
  );
  
  return $output;
}

/**
 * Página con detalle de instancia.
 */
function sorteosmag_instances_detall_page($instance) {

  $output = array();
  $result = _sorteosmag_instance_get($instance);
  $sorteo = node_load($result['sorteo_id']);
  $result['contestant'] = _sorteosmag_number_contestant($instance);
  $winners = FALSE;

  drupal_set_title(t('@title / @end_date', array(
    '@end_date' => $result['end_date'],
    '@title' => $sorteo->title)));

  // No mostramos lista de ganadores, cada caja de instancia
  // muestra los suyos.
  // if ( $result['status'] == SORTEOSMAG_INSTANCES_STATUS_END || 
  //   $result['status'] == SORTEOSMAG_INSTANCES_STATUS_WINNERS ||
  //   $result['status'] == SORTEOSMAG_INSTANCES_STATUS_CLOSE
  //   ) {
  //   $results_display_view = 'winners_of_instance';
  //   $winners = views_embed_view($results_display_view, 'default', $instance);
  // }

  // Form subscription.
  module_load_include('inc', 'sorteosmag', 'includes/sorteosmag_subscription');
  $form = drupal_get_form('sorteosmag_subscription_form_' . $instance, $instance);
  $form['#submit'] = 'sorteosmag_subscription_form';

  $output['Instance'] = array(
    '#markup' => theme('sorteosmag_instance_of_draw', array(
      'sorteo'  => $sorteo,
      'instance'  => $result,
      'status'     => sorteosmag_instances_status_array()[$result['status']],
      'winners' => $winners,
      'actions' => $form,
      )
    )
  );

  return $output;


}

/**
 * sorteosmag_add_contestant.
 */
function sorteosmag_add_contestant($form, &$form_state, $instance) {

  $form['#instances_id'] = $instance;

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Subscribe user to draw'),
  );
  $form['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#autocomplete_path' => 
      'sorteosmag/unique_user_autocomplete_callback',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel',
  );
  return $form;

}

/**
 * Validate form add_contestant.
 */ 
function sorteosmag_add_contestant_validate($form, &$form_state) {
  $instances_id = $form_state['complete form']['#instances_id'];
  $op = $form_state['values']['op'];
  if ($op == 'Cancel') {
    drupal_goto('sorteos/' . $instances_id . '/view');
  }
  $user = $form_state['values']['user'];
  if ( empty($user) ) {
    form_set_error('user', t('You must define a user to enroll.'));
  }
}

/**
 * Submit sorteosmag_add_instance.
 */
function sorteosmag_add_contestant_submit($form, &$form_state) {

  $user = $form_state['values']['user'];
  $instances_id = $form_state['complete form']['#instances_id'];

  $matches = array();
  $result = preg_match('/\[([0-9]+)\]$/', $user, $matches);
  $user_id = $matches[$result];

  $user = user_load($user_id);

  if ( _sorteosmag_insert_contestant($instances_id, $user_id) ) {

    drupal_set_message(t('The user @name has been registered.',
      array(
        '@name' => $user->name,
      )
    ));

  }

}

/**
 * Página con los concursantes de instancia.
 */
function sorteosmag_instances_contestants_page($instance) {

  drupal_set_title(t('Instance of draw'));

  $results_display_view = 'contestant_of_draw';
  $results = views_embed_view($results_display_view, 'default', $instance);

  return $results;
}

/**
 * Execute the draw.
 */
function sorteosmag_execute_form($form, &$form_state, $instance) {

  sorteosmag_cron();

  $instance = _sorteosmag_instance_get($instance);
  $sorteo = node_load($instance['sorteo_id']);

  drupal_set_title(t('Execute contest @title', array('@title' => $sorteo->title)));

  $form['#tree'] = TRUE;
  $form['#instance_id'] = $instance['id'];
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Define winners'),
  );

  // Comprobar estado de la instancia.
  if ( $instance['status'] != SORTEOSMAG_INSTANCES_STATUS_CLOSE &&
    $instance['status'] != SORTEOSMAG_INSTANCES_STATUS_WINNERS
   ) {
    drupal_set_message ( 'For the draw is made must be closed.', 'error' );
    drupal_goto('sorteos/' . $instance['id'] . '/view');
  }

  // Mirar si ya tenemos ganadores.
  $winners = _sorteosmag_winners_instance($instance['id']);
  if ( $winners ) {
    drupal_set_message(t('This action will overwrite the assigned winners.'));
    $winners_msg = FALSE;
    foreach ($winners as $winner) {
      $winner_user = user_load($winner->user_id);
      $prize = node_load($winner->prize_id);
      drupal_set_message(t('Winner: :winner_name Prize: :prize_title', array(
        ':winner_name' => $winner_user->name,
        ':prize_title' => $prize->title,
      )));
    }
  }

  // Select prizes of draw.
  $prizes = _sorteosmag_prizes_draw($instance['sorteo_id']);
  $count = 0;
  foreach ($prizes as $p ) {
    $prize_id = $p->prize_id;
    $prize_quantity = $p->quantity;
    $prize = node_load($prize_id);
    $count++;

    // Cantidad de premios.
    $form['sorteosmag']['winners'][$prize->nid] = array (
      '#type' => 'fieldset',
      '#title' => t('Winner of @prize_title', array('@prize_title' => $prize->title)),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $count_instances_prize = 0;
    for ($x = 0; $x < $prize_quantity; $x++) { 

      // $form['sorteosmag']['winners'][$count][$count_instances_prize] = array(
      //   '#type' => 'fieldset',
      //   '#title' => t('Winner of @prize_title', array('@prize_title' => $prize->title)),
      //   '#prize_id' => $prize->nid,
      // );

      $form['sorteosmag']['winners'][$prize_id][$count_instances_prize]['winner'] = array(
        '#type' => 'textfield',
        '#title' => t('Winner') . ': ' . ($count_instances_prize + 1 ),
        '#autocomplete_path' => 
          'sorteosmag/unique_contestant_autocomplete_callback/' . $instance['id'],
        // '#default_value' => $default_value
        // '#description' => t("Enter Prize."),
        // '#size' => 20,
        // '#maxlength' => 20,
        // '#required' => FALSE,
      );
      $count_instances_prize++;
    }
  }

  $form['#total_prizes'] = $count;
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['random'] = array(
    '#type' => 'submit',
    '#value' => t('Random'),
  );
  return $form;

}

/**
 * Validate form add_execute.
 */ 
function sorteosmag_execute_form_validate($form, &$form_state) {
  if ( $form_state['values']['op'] == t('Random')) {
    return TRUE;
  }
  $winners = $form_state['values']['sorteosmag']['winners'];
  if ( empty($winners) ) {
    form_set_error ( 'winners', t('It is necessary to define a user prize.'));
  }
  foreach ($winners as $winner ) {
    foreach ( $winner as $win ) {
      if ( empty($win) ) {
        form_set_error('winners', t('It is necessary to define a user prize.'));
      }
    }
  }
}

/**
 * Submit sorteosmag_add_instance.
 */
function sorteosmag_execute_form_submit($form, &$form_state) {

  $instance_id = $form['#instance_id'];
  $instance = _sorteosmag_instance_get($instance_id);
  $sorteo_id = $instance['sorteo_id'];

  // First delete old winners.
  db_update('sorteosmag_contestant')
    ->condition('instance_id', $instance_id)
    ->fields(array('prize_id' => NULL))
    ->execute();

  if ( $form_state['values']['op'] == t('Random')) {

    _sorteosmag_generate_winners_rand($instance_id, $sorteo_id);

    // $number_prizes = count($form['sorteosmag']['winners']);

    // $query = db_query("SELECT user_id FROM {sorteosmag_contestant} 
    //   WHERE instance_id = :instance_id ORDER BY RAND() LIMIT " . $number_prizes,
    //     array(':instance_id' => $instance_id));

    // foreach ( $query as $result ) {
    //   $winners_rand[] = $result->user_id;
    // }

    // $winners = $form_state['values']['sorteosmag']['winners'];
    // $count = 0;
    // foreach ( $winners as $user ) {
    //   $prize_id = $form['sorteosmag']['winners'][$count+1]['#prize_id'];
    //   $user_id = $winners_rand[$count];
    //   $user = user_load($user_id);
    //   if ( _sorteosmag_insert_winner($instance_id, $user_id, $prize_id) ) {
    //     drupal_set_message(t('The user @name has been registered like winner.',
    //       array(
    //         '@name' => $user->name,
    //       )
    //     ));
    //   }
    //   else {
    //     return FALSE;
    //   }
    //   $count++;
    // }

  }
  else {
    $winners = $form_state['values']['sorteosmag']['winners'];
    $count = 0;
    foreach ($winners as $prize_id => $win) {
      foreach ( $win as $user ) {
        $count++;
        // $prize_id = $form['sorteosmag']['winners'][$count]['#prize_id'];
        $matches = array();
        $result = preg_match('/\[([0-9]+)\]$/', $user['winner'], $matches);
        $user_id = $matches[$result];

        $user = user_load($user_id);

        if ( _sorteosmag_insert_winner($instance_id, $user_id, $prize_id) ) {

          drupal_set_message(t('The user @name has been registered like winner.',
            array(
              '@name' => $user->name,
            )
          ));
        }
        else {
          return FALSE;
        }
      }
    }
  }

  // Pasamos instancia a ganadores asignados.
  _sorteosmag_change_status_instance($instance_id, SORTEOSMAG_INSTANCES_STATUS_WINNERS);
  drupal_goto('sorteos/' . $instance_id . '/view');

}
