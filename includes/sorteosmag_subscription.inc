<?php

/**
 * @file
 */

/**
 * sorteosmag_suscription.
 */
function sorteosmag_subscription_form($form, &$form_state, $instance) {

  global $user;

  $form['#instances_id'] = $instance;

  if ( $user->uid ) {

    // Comprobar si ya participa.
    if ( _sorteosmag_is_contestant_instance($instance, $user->uid) ) {
      $form['contest'] = array(
        '#markup' => t('You are participating in the contest.'),
        '#weight' => 20,
        '#prefix' => '<p class="user-participating">',
        '#suffix' => '</p>',
      );
    }
    else {
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('I want to participate'),
        '#weight' => 20,
        '#prefix' => '<p class="participate">',
        '#suffix' => '</p>',
      );
    }

    return $form;

  } else {

    $form['contest'] = array(
      '#markup' => t('To participate you need to be registered.'),
    );
    $form['linklogin'] = array(
      '#markup' => '<p>' .
        l(t('Login'), 'user/login') .
        ' ' .
        t('or') .
        ' ' .
        l(t('Register'), 'user/register') .
        '</p>' ,
    );

    // drupal_set_message(t('You need to be registered to incribirse in the draw.'));

    return $form;
  }

}

/**
 * Validate form add_suscription.
 */ 
function sorteosmag_subscription_form_validate($form, &$form_state) {
  global $user;
  if ( empty($user) ) {
    form_set_error('submit', t('You need to log in to register.'));
    return;
  }
}

/**
 * Submit sorteosmag_add_instance.
 */
function sorteosmag_subscription_form_submit($form, &$form_state) {
  global $user;

  $instances_id = $form_state['complete form']['#instances_id'];

  drupal_set_message(t('The user @name has been registered.',
    array(
      '@name' => $user->name,
    )
  ));

  // module_load_include('inc', 'sorteosmag', 'includes/sorteosmag.api');

  if ( _sorteosmag_insert_contestant($instances_id, $user->uid) ) {

    drupal_set_message(t('Subscription accepted.',
      array(
        '@name' => $user->name,
      )
    ));

  }
}
