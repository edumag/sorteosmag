<?php
/**
 * @file
 */

/**
 * Implements hook_form_after().
 */
function sorteosmag_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'sorteosmag_node_form') {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => (isset($form_state['node']->title)) ? $form_state['node']->title : t('Title'),
      '#size' => 80,
      '#maxlength' => 255,
      '#required' => TRUE,
    );

    _sorteosmag_form_alter($form, $form_state, $form_id);

  }

}

/**
 * Implementation of hook_form_alter().
 */
function _sorteosmag_form_alter(&$form, &$form_state, $form_id) {

  $node = isset($form['#node']) ? $form['#node'] : FALSE ;

  $form['sorteosmag'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sorteo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="names-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  $form['sorteosmag']['automatic'] = array(
    '#type'=> 'checkbox',
    '#title' => t('Automatic'),
    '#description' => t('Automatically generate winners.'),
    '#default_value' => isset($node->sorteosmag['automatic'])
      ? $node->sorteosmag['automatic']
      : 1,
  );

  // $form['sorteosmag'] += array(
  //   '#weight' => module_exists('content') ? content_extra_field_weight($node->type, 'sorteosmag') : 1,
  //   '#tree' => TRUE,
  // );

  // Declaramos el formulario dinamico para poder crear tantos premios como se
  // quiera.
  $form['#tree'] = TRUE;
  $prizes = ( isset($node->sorteosmag['prizes']) ) 
    ? $node->sorteosmag['prizes'] 
    : FALSE;

  if (empty($form_state['sorteosmag']['num_prize'])) {
    if ( $prizes && count($prizes) > 1 ) {
      $form_state['sorteosmag']['num_prize'] = count($prizes);
    } else {
      $form_state['sorteosmag']['num_prize'] = 1;
    }
    }

  // Forzamos orden de los campos al presentarse.
  $form['sorteosmag']['prize'] = array(
    '#weight' => 15,
  );

  // Build the number of prize fieldsets indicated by $form_state['num_prize']
  for ($i = 1; $i <= $form_state['sorteosmag']['num_prize']; $i++) {
    $form['sorteosmag']['prize'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Prize #@num', array('@num' => $i)),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    if ( isset($prizes[$i-1]) ) {
      $node_id = $prizes[$i-1]['prize'];
      $node = node_load($node_id);
      $default_value = $node->title . ' [' . $node_id . ']';
      $default_quantity = $prizes[$i-1]['quantity'];

    } else {
      $default_value = '';
      $default_quantity = 1;
    }

    $form['sorteosmag']['prize'][$i]['prize'] = array(
      '#type' => 'textfield',
      '#title' => t('Prize'),
      '#autocomplete_path' => 
        'sorteosmag/unique_node_autocomplete_callback',
      '#default_value' => $default_value
      // '#description' => t("Enter Prize."),
      // '#size' => 20,
      // '#maxlength' => 20,
      // '#required' => FALSE,
    );
    $form['sorteosmag']['prize'][$i]['quantity'] = array(
      '#type' => 'textfield',
      '#title' => t('Qunatity'),
      '#size' => 3,
      '#default_value' => $default_quantity
    );
  }

  // Adds "Add another prize" button
  $form['sorteosmag']['add_prize'] = array(
    '#type' => 'submit',
    '#weight' => 16,
    '#value' => t('Add another prize'),
    '#submit' => array('sorteosmag_form_add_prize'),
    // See the examples in ajax_example.module for more details on the
    // properties of #ajax.
    '#ajax' => array(
      'callback' => 'sorteosmag_add_more_callback',
      'wrapper' => 'names-fieldset-wrapper',
    ),
  );

  // If we have more than one prize, this button allows removal of the
  // last prize.
  if ($form_state['sorteosmag']['num_prize'] > 1) {
    $form['sorteosmag']['remove_prize'] = array(
      '#type' => 'submit',
      '#weight' => 17,
      '#value' => t('Remove latest prize'),
      '#submit' => array('sorteosmag_form_remove_prize'),
      // Since we are removing a prize, don't validate until later.
      // '#limit_validation_errors' => array(),
      '#ajax' => array(
        'callback' => 'sorteosmag_add_more_callback',
        'wrapper' => 'names-fieldset-wrapper',
      ),
    );
  }
}

/**
 * AJAX callback to attach the message type fields to the form.
 *
 * Since the controlling logic for populating the form is in the form builder
 * function, all we do here is select the element and return it to be updated.
 */
function sorteosmag_add_more_callback(array $form, array &$form_state) {
  return $form['sorteosmag'];
}
/**
 * Submit handler for "Add another prize".
 */
function sorteosmag_form_add_prize($form, &$form_state) {
  $form_state['sorteosmag']['num_prize']++;

  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

function sorteosmag_form_remove_prize($form, &$form_state) {
  if ($form_state['sorteosmag']['num_prize'] > 1) {
    $form_state['sorteosmag']['num_prize']--;
  }

  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Autocomplete callback for nodes by title.
 *
 * Searches for a node by title, but then identifies it by nid, so the actual
 * returned value can be used later by the form.
 *
 * The returned $matches array has
 * - key: The title, with the identifying nid in brackets, like "Some node
 *   title [3325]"
 * - value: the title which will is displayed in the autocomplete pulldown.
 *
 * Note that we must use a key style that can be parsed successfully and
 * unambiguously. For example, if we might have node titles that could have
 * [3325] in them, then we'd have to use a more restrictive token.
 *
 * @param string $string
 *   The string that will be searched.
 */
function sorteosmag_unique_node_autocomplete_callback($string = "") {
  $matches = array();
  if ($string) {
    $result = db_select('node')
      ->fields('node', array('nid', 'title'))
      ->condition('node.type', 'magprizes')
      ->condition('title', db_like($string) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();
    foreach ($result as $node) {
      $matches[$node->title . " [$node->nid]"] = check_plain($node->title);
    }
  }

  drupal_json_output($matches);
}
function sorteosmag_unique_contestant_autocomplete_callback($instance_id, $string = "") {
  $matches = array();
  // $instance_id = 1;

  if ($string) {
    $query = db_select('users');
    $query->innerjoin('sorteosmag_contestant', 'sc', 'users.uid = sc.user_id');
    $query->fields('users', array('uid', 'name'));
    $query->fields('sc', array('instance_id'));
    $query->condition('name', '%' . db_like($string) . '%', 'LIKE');
    $query->condition('sc.instance_id', $instance_id);
    $query->range(0, 10);
    $result = $query->execute();
    foreach ($result as $user) {
      $matches[$user->name . " [$user->uid]"] = check_plain($user->name);
    }
  }

  drupal_json_output($matches);
}

function sorteosmag_unique_user_autocomplete_callback($string = "") {
  $matches = array();
  if ($string) {
    $result = db_select('users')
      ->fields('users', array('uid', 'name'))
      ->condition('name', '%' . db_like($string) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();
    foreach ($result as $user) {
      $matches[$user->name . " [$user->uid]"] = check_plain($user->name);
    }
  }

  drupal_json_output($matches);
}
