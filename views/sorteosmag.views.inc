<?php
/**
 * @file
 * Implements the sorteo view.
 *
 * @defgroup sorteosmag_view_data Sorteo View
 * @ingroup sorteosmag
 * @{
 * View from sorteosmag.
 */

/**
 * Implements hook_views_data().
 */
function sorteosmag_views_data() {
  $data['sorteosmag']['table']['group'] = t('Sorteo');
  $data['sorteosmag']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Id of Sorteo'),
    'help' => t('Sorteos view.'),
    'weight' => -10,
  );
  $data['sorteosmag']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['sorteosmag']['id'] = array(
    'title' => t('Id of SorteoMag'),
    'help' => t('Sorteo identifier.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['sorteosmag']['nid'] = array(
    'title' => t('Id of Node Sorteo'),
    'help' => t('Id of Node sorteo.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id of node in SoretosMag'),
      'title' => t('Id of node in SorteosMag'),
      'help' => t('More information on this relationship'),
    ),
  );

  // Prizes
  $data['sorteosmag_prize']['table']['group'] = t('Sorteo');
  $data['sorteosmag_prize']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Prizes of Sorteos'),
    'help' => t('Prize view.'),
    'weight' => -10,
  );
  $data['sorteosmag_prize']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'sorteo_id',
    ),
  );
  $data['sorteosmag_prize']['sorteo_id'] = array(
    'title' => t('Sorteo Id'),
    'help' => t('Id of node Sorteo in Prizes.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id of node Sorteo in Prizes'),
      'title' => t('Id of node Sorteo in Prizes'),
      'help' => t('More information on this relationship'),
    ),
  );
  $data['sorteosmag_prize']['prize_id'] = array(
    'title' => t('Prize Id'),
    'help' => t('Prize identifier.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id of prize in sorteosmag prize'),
      'title' => t('Id of prize in sorteosmag prize'),
      'help' => t('More information on this relationship'),
    ),
  );
  $data['sorteosmag_prize']['quantity'] = array(
    'title' => t('Quantity of Prizes'),
    'help' => t('Quantity od prizzes.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  // Concursantes
  $data['sorteosmag_contestant']['table']['group'] = t('Sorteo');
  $data['sorteosmag_contestant']['table']['base'] = array(
    'field' => 'instance_id',
    'title' => t('Id of draw instance'),
    'help' => t('Contestant view.'),
    'weight' => -10,
  );
  $data['sorteosmag_contestant']['table']['join'] = array(
    'sorteosmag_instance' => array(
      'left_field' => 'id',
      'field' => 'instance_id',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'user_id',
    ),
    'node' => array(
      'left_field' => 'nid',
      'field' => 'prize_id',
    ),
  );
  $data['sorteosmag_contestant']['instance_id'] = array(
    'title' => t('Instance Id in table of contestants'),
    'help' => t('Instance identifier.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'sorteosmag_instance',
      'base field' => 'id',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id Instance of sorteo in contestants'),
      'title' => t('Id Instance of sorteo in contestants'),
      'help' => t('More information on this relationship'),
    ),
  );
  $data['sorteosmag_contestant']['user_id'] = array(
    'title' => t('User Id in table of contestants'),
    'help' => t('Concursante identifier.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id User contestant in instance'),
      'title' => t('Id User contestant in instance'),
      'help' => t('More information on this relationship'),
    ),
  );
  $data['sorteosmag_contestant']['prize_id'] = array(
    'title' => t('Prize id: Id of node prize'),
    'help' => t('Prize of winner.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id Node prize of winner'),
      'title' => t('Id Node prize of winner'),
      'help' => t('More information on this relationship'),
    ),
  );
  // Instances
  $data['sorteosmag_instance']['table']['group'] = t('Sorteo');
  $data['sorteosmag_instance']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Instances of Sorteos'),
    'help' => t('Instance view.'),
    'weight' => -10,
  );
  $data['sorteosmag_instance']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'sorteo_id',
    ),
  );
  $data['sorteosmag_instance']['sorteo_id'] = array(
    'title' => t('Id Node Sorteo in Instances'),
    'help' => t('Id Node Sorteo in Instances.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      // 'field' => 'nid' -- see hook_views_data_alter(); not needed here.
      'handler' => 'views_handler_relationship',
      'label' => t('Id of sorteo node in Instances of Sorteo'),
      'title' => t('Id of sorteo node in Instances of Sorteo'),
      'help' => t('More information on this relationship'),
    ),
  );
  $data['sorteosmag_instance']['id'] = array(
    'title' => t('Instance Id'),
    'help' => t('Instance identifier.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['sorteosmag_instance']['start_date'] = array(
    'title' => t('start_date'),
    'help' => t('start_date instance of sorteo.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['sorteosmag_instance']['end_date'] = array(
    'title' => t('end_date'),
    'help' => t('end_date instance of sorteo.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['sorteosmag_instance']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status instance of sorteo.'),
    'field' => array(
      'handler' => 'instances_status_handler',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
/**
 * Custom handler of status instance.
 */
class instances_status_handler extends views_handler_field {

  function render($values) {

    $status = $values->sorteosmag_instance_status;
    if ( !$status ) return '';
    $instances_status_array = sorteosmag_instances_status_array();
    if ( isset($instances_status_array[$status]) ) {
      return $instances_status_array[$status];
    }
  }
}
/**
 * @} End of "defgroup sorteo_view".
 */

