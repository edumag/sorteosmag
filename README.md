# SorteosMag

## Creating draws.

When installing the module, the content type "sorteosmag" is created.

We can customize it to our liking by adding, for example, a field
of image.

With prizes we do the same are nodes of type magprize that can be
personalize.

The list of the prizes that shows when seeing a draw is made with the view
Sorteosmag_prizes also allows to be personalized.

## Development

If we want to do tests we can generate content directly with drush.

	drush sorteosmag_create

### Reinstall module and include demo content.

	# Uninstall.
	drush -y dis sorteosmag_repeat sorteosmag_rules sorteosmag_userpoints ; drush -y dis sorteosmag ; 
	drush pm-uninstall -y sorteosmag_repeat sorteosmag_rules sorteosmag_userpoints ; drush pm-uninstall -y sorteosmag ; 
	# Install.
	drush -y en sorteosmag sorteosmag_repeat sorteosmag_rules sorteosmag_userpoints ;  
 	# Add demo content.
	drush sorteosmag_create

