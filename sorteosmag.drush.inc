<?php

/**
 * @file
 *   sorteosmag module drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function sorteosmag_drush_command() {
  $items = array();
 
  $items['sorteosmag'] = array(
    'description' => 'List all sorteos.',
    'aliases' => array('smg'),
  );
 
  $items['sorteosmag_create'] = array(
    'description' => 'Create demo content.',
    'aliases' => array('smgc'),
  );
  $items['sorteosmag_delete'] = array(
    'description' => 'Borrado de contenido.',
    'aliases' => array('smgd'),
  );
 
  return $items;
}
/**
 * Implements hook_drush_help().
 */
function sorteosmag_drush_help($section) {
  switch ($section) {
    case 'drush:sorteosmag':
      return dt("List all the available sorteos for your site.").
        "\n".
        "\nOther commads:".
        "\n".
        "\nsorteosmag_create Create a demo content.".
        "\n".
        "\n  This action delete old content.".
        "\n".
        "\nsorteosmag_delete Delete all content of sorteosmag";
    case 'drush:sorteosmag_create':
      return dt("Create s demo content.");
    case 'drush:sorteosmag-delete':
      return dt("Delete all content od sorteosmag.");
  }
}
function drush_sorteosmag_delete() {

  drush_print();
  drush_print('Borrado de contenido');


  // Borrar nodos
  drush_shell_exec("drush genc --kill --types=magprizes 0 0");
  drush_print(implode("\n", drush_shell_exec_output()));
  drush_shell_exec("drush genc --kill --types=sorteosmag 0 0");
  drush_print(implode("\n", drush_shell_exec_output()));

  // Borrar contenido de tablas.
  $tablas = array(
    'sorteosmag',
    'sorteosmag_contestant',
    'sorteosmag_instance',
    'sorteosmag_prize',
  );

  foreach( $tablas as $tabla ) {
    db_query("DELETE FROM $tabla");
    drush_print(dt('Delete all reg of table @table', array('@table' => $tabla)));
  }

}
function drush_sorteosmag() {

  $result = db_query("SELECT DISTINCT * FROM {sorteosmag_instance}");
  foreach ( $result as $inst) {
    $node = node_load($inst->sorteo_id);
    $autor = user_load($node->uid);
    drush_print('');
    drush_print(dt('Title: !title [!nid] Date: !end_date', 
      array(
        '!title' => $node->title,
        '!nid' => intval($node->nid),
        '!end_date' => $inst->end_date
      )));
    drush_print(dt('  Automatic: !automatic Status: !status', 
      array(
        '!automatic' => $node->sorteosmag['automatic'],
      )));
    drush_print(dt('  Author: !author', 
      array('!author' => $autor->name)));
    drush_print(dt('  Instance ID: !instance_id Status: !status Contestants: !contestants', 
      array(
        '!instance_id' => $inst->id,
        '!status' => sorteosmag_instances_status_array()[$inst->status],
        '!contestants' => $node->sorteosmag['instances'][0]['num:contestants'],
      )));
    drush_print(dt('  Start Date:  !start_date', 
      array('!start_date' => $inst->start_date)));
    // drush_print(dt('  End Date:  !end_date', 
    //   array('!end_date' => $inst->end_date)));
    foreach($node->sorteosmag['prizes'] as $prize ) {
      $prize = node_load($prize);
      drush_print(dt('  Prize:  !prize', 
        array('!prize' => $prize->title)));
    }
    if (isset($node->sorteosmag['instances'][0]['winners']) && !empty($node->sorteosmag['instances'][0]['winners'])) {
      foreach($node->sorteosmag['instances'][0]['winners'] as $winner ) {
        $winner_id = $winner['winner'];
        $price_id  = $winner['prize']; 
        $winner = user_load($winner_id);
        $prize = node_load($price_id);
        drush_print(dt('  Winner:  !winner Prize: !prize', 
          array(
            '!winner' => $winner->name,
            '!prize' => $prize->title,
          )));
      }
    }
  }
}
function drush_sorteosmag_create() {

  drush_print();
  drush_print(dt('SoretosMag'));
  drush_print(dt('----------'));
  drush_print();

  if (!module_exists('devel_generate')) {
    drush_set_error(dt("It's necessary. have enabled module devel_generate."));
    return;
  }
  drush_sorteosmag_delete();
  _sorteosmag_drush_create_demo();

}
function _sorteosmag_drush_create_demo(){

  $users_demo   = FALSE;
  $premios_demo = FALSE;
  $sorteos_demo = FALSE;

  // Crear usuarios.
  drush_shell_exec("drush generate-users 50");
  drush_print(implode("\n", drush_shell_exec_output()));

  // Recoger ids de usuarios creados.
  $query = db_query('SELECT uid FROM users ORDER BY created desc LIMIT 50');
  foreach ($query as $user){
    $users_demo[] = $user->uid;
  }

  // Crear premios.
  $type = 'magprizes';
  $number = 5;
  drush_shell_exec("drush generate-content %d --types=%s", $number, $type);
  drush_print(implode("\n", drush_shell_exec_output()));

  // Recoger id de premios creados.
  $query = db_query('SELECT nid FROM node ORDER BY created desc LIMIT ' . $number);
  foreach ($query as $premios){
    $premios_demo[] = $premios->nid;
  }

  // Crear sorteosmag.
  $type = 'sorteosmag';
  $number = 2;
  drush_shell_exec("drush generate-content %d --types=%s", $number, $type);
  drush_print(implode("\n", drush_shell_exec_output()));

  // Recoger id de sorteos creados.
  $query = db_query('SELECT nid FROM node ORDER BY created desc LIMIT ' . $number);
  foreach ($query as $sorteos){
    $sorteos_demo[] = $sorteos->nid;
  }

  // Relacionar Sorteos con premios.
  $count = 0;
  foreach($sorteos_demo as $node_id) {
    $sorteo = node_load($node_id);
    $premio1 = node_load($premios_demo[$count]);
    $premio2 = node_load($premios_demo[$count+1]);
    $sorteo->sorteosmag = array();
    $sorteo->sorteosmag['status'] = 1;
    $sorteo->sorteosmag['automatic'] = $count;
    $sorteo->sorteosmag['prize'][0]['prize'] = "$premio1->title [".$premio1->nid."]";
    $sorteo->sorteosmag['prize'][0]['quantity'] = 1;
    $sorteo->sorteosmag['prize'][1]['prize'] = "$premio2->title [".$premio2->nid."]";
    $sorteo->sorteosmag['prize'][1]['quantity'] = 10;
    $sorteo->sorteosmag['userpoints'] = 100;
    sorteosmag_node_update($sorteo);
    sorteosmag_userpoints_node_update($sorteo);
    drush_print();
    drush_print('Node: '.$sorteo->title);
    drush_print('  Prize: '.$premio1->title);
    drush_print('  Prize: '.$premio2->title);
    $count++;
  }
  // Crear instancias.
  $total_instances_x_sorteo = 5;

  drush_print();
  drush_print('Creación de instancias, numero: '. $total_instances_x_sorteo . ' x nodo');
  drush_print();
  foreach($sorteos_demo as $node_id) {
    for($count=0;$count<$total_instances_x_sorteo;$count++) {
      $start_date = time()-(60 * 60 * 24 * $count - $total_instances_x_sorteo);
      $end_date   = $start_date+(60 * 60 * 24 * 5);
      $start_date = date ( "Y-m-d h:i:s" , $start_date );
      $end_date   = date ( "Y-m-d h:i:s" , $end_date );
      $values['startdate'] = $start_date;
      $values['enddate']   = $end_date;
      _sorteosmag_insert_instance($node_id, $values);
      drush_print('Node: '.$node_id);
      drush_print('  Instance: '.$start_date. ' / ' . $end_date);
    }
  }

  // Recoger id de instancias de sorteos creados.
  $query = db_query('SELECT id FROM {sorteosmag_instance} ORDER BY id desc LIMIT 2');
  foreach ($query as $instancias){
    $instancias_demo[] = $instancias->id;
  }

  // Añadir concursantes.
  foreach( $instancias_demo as $instance_id){
    foreach($users_demo as $user_id){
      _sorteosmag_insert_contestant($instance_id, $user_id); 
      drush_print('Contestant Instance: '.$instance_id. ' / user: ' .$user_id);
    }
  }
}
function _sorteosmag_drush_create_node($type, $data, $user = 1){

  // you need this to autoload the functions to create nodes
  module_load_include('inc', 'node', 'node.pages');

  foreach( $data as $res ){

    // define the custom node type
    $node = (object) array(
      'uid'      => $user,
      // 'name'     => 'drush',
      'type'     => $type,
      'language' => 'und',
    );
    node_object_prepare($node);
    $node->uid = 1;

    // set up the form array
    $form_state = array();

    // Get default values from attached fields.
    $fields = field_info_instances('node');
    foreach($fields[$type] as $field_name => $values) {
      $form_state['values'][$field_name] = array('und' => array());
    }

    // set the title of the node   
    $form_state['values']['title'] = $res["title"];
    $form_state['values']['body'] = $res["body"];
    $form_state['values']['options']['status'] = 1; //publish all imported nodes

    $form_state['values']['op'] = t('Save');
    drupal_form_submit('sorteosmag_node_form', $form_state, (object)$node);
    $errors = form_get_errors();
    if (!empty($errors)) {
      drush_print($errors);
    }

    drush_print('');
    drush_print(dt('Node ID: !nid', array('!nid' => intval($node->nid))));
    drush_print(dt('Title:   !title', array('!title' => $node->title)));
    drush_print(dt('Author:  !author', array('!author' => $node->author)));
  }
}
