<?php

/**
 * @file
 * Theme and template preprocessing code
 */


/**
 * Theme the simple contest node title
 */
//   function theme_sorteosmag_on_participant_node($vars) {
//   	
//   	$node = $vars['node'];
//   
//   	$content['contest'] = array(
//       '#type' => 'fieldset',
//       '#title' => t('Contest'),
//       '#collapsible' => TRUE,
//       '#collapsed' => FALSE,
//   	);
//   	$content['contest']['node'] = array(
//   		'#value' => "<h2>" . check_plain($node->title) . "</h2>" . node_show($node, TRUE, FALSE),
//   	);
//   	$output = drupal_render($content);
//   
//   	return $output;
//   }

/**
 * Theme the simple contest results_display
 */
function theme_sorteosmag_details_display($vars) {
	
	$node = $vars['node'];
	$status = $vars['status'];
	$results = $vars['results'];
	$instances = $vars['instances'];
	
  if (sorteosmag_access('administer sorteos')) {
    $automatic = (
      isset($node->sorteosmag['automatic']) && 
      $node->sorteosmag['automatic'] == 1
    ) ? 1 : 0 ;
    $content['status'] = array(
      '#markup' => "<strong>" . t('Automatic') . ": </strong>" . $automatic, 
      '#weight' => -10,
    );
  }

  if ( $results ) {
    $content['sorteosmag_prizes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Draw prizes'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 0,
    );
    $content['sorteosmag_prizes']['prizes'] = array(
      '#markup' => $results,
    );
  }

  if ( $instances ) {
    $content['sorteosmag_instances'] = array(
      // '#type' => 'fieldset',
      // '#title' => t('Instances'),
      // '#collapsible' => TRUE,
      // '#collapsed' => FALSE,
      '#weight' => 20,
    );
    foreach ($instances as $instance ) {
      $instance_id = $instance['instances_id'];
      $status = ($instance['status']) ? $instance['status'] : 0 ;
      $start_date = $instance['start_date'];
      $end_date = $instance['end_date'];
      $num_contestants = $instance['num_contestants'];
      $winners = $instance['winners'];
      if ( $status == SORTEOSMAG_INSTANCES_STATUS_OPEN ) {
        // Form subscription.
        module_load_include('inc', 'sorteosmag', 'includes/sorteosmag_subscription');
        $form = drupal_get_form('sorteosmag_subscription_form_' . $instance_id, $instance_id);
        $form['#submit'] = 'sorteosmag_subscription_form';
        $content['sorteosmag_instances' . $instance_id]['#type'] = 'fieldset';
        $content['sorteosmag_instances' . $instance_id]['#weight'] = 20;
        $content['sorteosmag_instances' . $instance_id]['#title'] = 
          '<strong>' . t('Start date') . ':</strong> ' . $start_date
          . ' / ' 
          . '<strong>' . t('End date') . ':</strong> ' . $end_date
          ;
        $content['sorteosmag_instances' . $instance_id][] = $form;

      }
      else {
        $instances_status_array = sorteosmag_instances_status_array();
        $status_literal = $instances_status_array[$status];
        $content['sorteosmag_instances' . $instance_id]['#type'] = 'fieldset';
        $content['sorteosmag_instances' . $instance_id]['#weight'] = 30;
        $content['sorteosmag_instances' . $instance_id]['#title'] = 
          '<strong>' . t('Start date') . ':</strong> ' . $start_date
          . ' / ' 
          . '<strong>' . t('End date') . ':</strong> ' . $end_date
          ;
        $content['sorteosmag_instances' . $instance_id][] = array(
          '#markup' => $status_literal, 
          '#prefix' => '<p class="status-' . $status . '">',
          '#suffix' => '</p>',
        );
        $content['sorteosmag_instances' . $instance_id][] = array(
          '#markup' => t('Number of participants') . '.' . $num_contestants,
          '#prefix' => '<p class="num_contestants">',
          '#suffix' => '</p>',
        );
        if ( $winners ) {
          $winners_list = FALSE;
          foreach ( $winners as $winner ) {
            $w = user_load($winner['winner']);
            $p = node_load($winner['prize']);
            $winners_list .= '<p>' 
              . '<strong>' . $w->name . '</strong>'
              . '<br/>' . 'Prize: '. $p->title
              . '</p>';
          }
          $content['sorteosmag_instances' . $instance_id][] = array(
            '#markup' => t('Winners') . '.' . $winners_list,
            '#prefix' => '<p class="sorteosmag_winners">',
            '#suffix' => '</p>',
          );
        }
      }
    }
  }

	
	$output = drupal_render($content);

	return $output;
}

/**
 * Theme instances of draw.
 */
function theme_sorteosmag_instances_display($vars) {
	
	$node = $vars['node'];
	$results = $vars['results'];
	// $status = $vars['status'];
	
  // $content['status'] = array(
  //   '#markup' => "<strong>" . t('Status of Draw') . ": </strong>" . $status, 
	//   '#weight' => -10,
  // );

  $content['sorteosmag_instances'] = array(
    '#type' => 'fieldset',
    '#title' => t('Draw instances'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $content['sorteosmag_instances']['instances'] = array(
    '#markup' => $results,
  );
	
	$output = drupal_render($content);

	return $output;
}


/**
 * Theme instance of draw.
 */
function theme_sorteosmag_instance_of_draw($vars) {
	
	$sorteo        = $vars['sorteo'];
	$instance      = $vars['instance'];
  $status        = $vars['status'];
  $winners       = $vars['winners'];
  $actions       = $vars['actions'];
	
  $content['sorteo_node'] = node_view($sorteo);
  $content['sorteo_node']['#weight'] = -100;

  // $content['sorteo_id'] = array(
  //   '#markup' => "<p><strong>" . t('Draw') . ": </strong>" . $sorteo->title . '</p>', 
	//   '#weight' => -10,
  // );
  $content['end_date'] = array(
    '#markup' => "<p><strong>" . t('Date of draw') . ": </strong>" . $instance['end_date'] . '</p>', 
	  '#weight' => -6,
  );

  if ( $winners ) {
    $content['winners'] = array(
      '#type' => 'fieldset',
      '#title' => t('Winners'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => -3,
    );
    $content['winners']['view'] = array(
      '#markup' => $winners,
    );
  }

  // $content['suscription'] = array(
  //   '#prefix' => '<p>',
  //   '#markup' => l(t('Contest'), 'sorteos/' . $instance['id'] . '/contest'),
  //   '#suffix' => '</p>',
	//   '#weight' => 20,
  // );

  if ( sorteosmag_access('administer sorteos') ) {
    $content['start_date'] = array(
      '#markup' => "<p><strong>" . t('Start Date') . ": </strong>" . $instance['start_date'] . '</p>', 
      '#weight' => -8,
    );
    $content['status'] = array(
      '#markup' => "<p><strong>" . t('Status') . ": </strong>" . $status . '</p>', 
      '#weight' => -9,
    );
    $content['contestant'] = array(
      '#markup' => "<p><strong>" . t('Numbre of contestant') . ": </strong>" . $instance['contestant'] . '</p>', 
      '#weight' => -5,
    );
    $content['add_new_contestant'] = array(
      '#prefix' => '<span class="button-sorteo">',
      '#markup' => l(t('Add new contestant'), 'admin/sorteos/instances/' . $instance['id'] . '/add_constestant'),
      '#suffix' => '</span>',
      '#weight' => 20,
    );
    $content['view_contestant'] = array(
      '#prefix' => '<span class="button-sorteo">',
      '#markup' => l(t('View contestants'), 'admin/sorteos/instances/' . $instance['id'] . '/contestants'),
      '#suffix' => '</span>',
      '#weight' => 20,
    );
    $content['edit_instance'] = array(
      '#prefix' => '<span class="button-sorteo">',
      '#markup' => l(t('Edit instance'), 'sorteos/' . $instance['id'] . '/edit'),
      '#suffix' => '</span>',
      '#weight' => 22,
    );
    $content['execute'] = array(
      '#prefix' => '<span class="button-sorteo">',
      '#markup' => l(t('Execute draw'), 'admin/sorteos/instances/' . $instance['id'] . '/execute'),
      '#suffix' => '</span>',
      '#weight' => 22,
    );
  }

  $content['actions'] = $actions;
  $content['actions']['#weight'] = 50;

	$output = drupal_render($content);

	return $output;
}


/**
 * Theme instance of suscription.
 */
function theme_sorteosmag_suscription($vars) {
	
	// $sorteo        = $vars['sorteo'];
	$instance      = $vars['instance'];
  // $status        = $vars['status'];
  // $results_prize = $vars['results_prize'];
	
  $link = l('Login', 'user/login', array('attributes' => array('rel'=>'nofollow')) );

  $content['instance'] = array(
    '#markup' => "<p><strong>" . t('Instance') . ": </strong>" . $instance . '</p>', 
	  '#weight' => -10,
  );
  $content['login'] = array(
    '#markup' => '<p>' . $link . '</p>', 
	  '#weight' => 10,
  );
	$output = drupal_render($content);

	return $output;
}
