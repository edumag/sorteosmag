<?php
/**
 * @file
 *
 * sorteosmag.token.inc
 */

/**
 * Implementation of hook_token_list().
 */
function sorteosmag_token_info() {

  // token types
  
  $info['types']['sorteo-node'] = array(
    'name' => t('Sorteo node'),
    'description' => t('Tokens related to sorteo node.'),
    'type' => 'node',
    'needs-data' => 'node',
  );
  
  $info['types']['prize-node'] = array(
    'name' => t('Prize node'),
    'description' => t("Tokens related to the prize node."),
    'type' => 'node',
    'needs-data' => 'node',
  );

  $info['types']['contestant-user'] = array(
    'name' => t('Participant user or Winner user'),
    'description' => t("Tokens related to the participant or winner user."),
    'type' => 'user',
    'needs-data' => 'user',
  );
  
  $info['types']['sorteo-instance'] = array(
    'name' => t('Instance of Sorteo'),
    'description' => t("Tokens related to instance of sorteo."),
  );
  
  
  // tokens
  
  $info['tokens']['sorteo-node']['max-winners'] = array(
    'name' => t('Max winners'),
    'description' => t("The max winners of this contest."),
  );
  
  $info['tokens']['sorteo-node']['winners-count'] = array(
    'name' => t('Winners count'),
    'description' => t("The total number of winners selected for this contest."),
  );
  
  $info['tokens']['sorteo-instance']['id'] = array(
    'name' => t('Identifiquer of instance'),
    'description' => t("Identifiquer of instance."),
  );
  
  $info['tokens']['sorteo-instance']['status'] = array(
    'name' => t('Status of instance'),
    'description' => t("Status of instance."),
  );
  
  $info['tokens']['sorteo-instance']['start_date'] = array(
    'name' => t('Start date of instance'),
    'description' => t("Start date of instance."),
  );
  
  $info['tokens']['sorteo-instance']['end_date'] = array(
    'name' => t('End date of instance'),
    'description' => t("End date of instance."),
  );
  
  return $info;
  
}


/**
 * Implementation of hook_token_values().
 */
function sorteosmag_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);
  
  switch ($type){
    
    case 'sorteo-instance':
      if ( !empty($data['sorteo-instance']) ) {
        $instance = $data['sorteo-instance'];
        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'status':
              $statuses = sorteosmag_instances_status_array();
              $replacements[$original] = $statuses[$instance['status']];
              break;
            default:
              $replacements[$original] = $data['sorteo-instance'][$name];
              break;
          }
        }
      }
      break;
    case 'sorteo-node':
      if ( !empty($data['sorteo-node']) ) {
        $sorteo = $data['sorteo-node'];
        $settings = sorteosmag_get_settings();
        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'winners-count':
              $replacements[$original] = $sorteo['sorteosmag']['winners_count'];
              break;
            default:
              $replacements[$original] = $data['sorteo-node']->$name;
              break;
          }
        }
      }
      break;
    case 'prize-node':
      if ( !empty($data['prize-node']) ) {
        $prize = $data['prize-node'];
        $settings = sorteosmag_get_settings();
        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'max-winners':
              $replacements[$original] = $settings['max_winners'];
              break;
            case 'winner-type':
              $replacements[$original] = $settings['winner_type'];
              break;
            case 'winners-count':
              $replacements[$original] = $prize['sorteosmag']['winners_count'];
              break;
            default:
              $replacements[$original] = $data['prize-node']->$name;
              break;
          }
        }
      }
      break;
    case 'contestant-user':
      if ( !empty($data['contestant-user']) ) {
        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'participant-rank':
              $replacements[$original] = $data['participant-rank'];
              break;
            default:
              $replacements[$original] = $data['contestant-user']->$name;
              break;

          }
        }
      }
      break;    
    }
    
  return $replacements;

}

